import 'package:flutter/material.dart';
import 'master.dart';

class Reference extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    //title
    Widget title = new Container(
      padding: const EdgeInsets.all(20.0),
      child: new Row(
        children: [
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new Text(
                    'D&D 5e Reference Guide',
                    style: new TextStyle(
                      fontSize: 26.0,
                    ),
                  ),
                ),
              ]
            )
          ),
        ],
      ),
    );

    List<Widget> info = [];

    info.add(title);

    return new Master(
      title: 'Reference',
      child: new ListView(
        children: info,
      ), 
    );
  }
}

class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;
}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty)
      return new Padding(
        padding: new EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 20.0),
        child: new Text(
          root.title,
          textAlign: TextAlign.left,
          style: new TextStyle(
        
          ),
        ),
      );
    return new ExpansionTile(
      key: new PageStorageKey<Entry>(root),
      initiallyExpanded: true,
      title: new Text(
          root.title,
          style: new TextStyle(
            color: redTheme,
            fontWeight: FontWeight.w600,
          ),
        ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}