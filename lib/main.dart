import 'package:flutter/material.dart';

import 'characters.dart';
import 'reference.dart';

void main() => runApp(
  new MyApp()
);

final redTheme = Colors.redAccent[100];
final redThemeLighter = Colors.red[100];
final offwhiteTheme = Colors.grey[50];

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Reference(),
      routes: <String, WidgetBuilder> {
        '/characters': (BuildContext context) => new Characters(),
      },
      title: 'Tomes',
      theme: new ThemeData(
        primarySwatch: Colors.red,
        accentColor: Colors.black,
        dividerColor: Colors.grey[350],
      ),
    );
  }
}

