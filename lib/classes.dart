import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';

import 'master.dart';

class Classes extends StatefulWidget {
  @override
  _ClassesState createState() => new _ClassesState(name: name, type: type);

  Classes({
    this.name,
    this.type,
  });

  final String name;
  final String type;
}

class _ClassesState extends State<Classes> {

  _ClassesState({
    this.name,
    this.type,
  });

  final String name;
  final String type;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final reference = FirebaseDatabase.instance.reference().child(this.type);

    Widget title = new Container(
      padding: const EdgeInsets.all(20.0),
      child: new Row(
        children: [
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: new Text(
                    this.name,
                    style: new TextStyle(
                      fontSize: 32.0,
                    ),
                  ),
                ),
                new Text(
                  this.type.substring(0, 1).toUpperCase() + this.type.substring(1),
                  style: new TextStyle(
                    color: Colors.grey[500],
                    decoration: TextDecoration.underline,
                  ),
                ),
              ]
            )
          ),
        ],
      ),
    );

    List<Widget> info = [];

    info.add(title);

    switch(this.type) {
      case "races":
        return new Master(
          title: name,
          child: new FirebaseAnimatedList(
            query: reference.orderByChild('name').equalTo(this.name),
            reverse: false,
            itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {

              var traits = snapshot.value['traits'];

              var subItems = new List<Widget>.generate(traits.length, (int index) {
                return new EntryItem(new Entry(traits[index]['name'], traits[index]['description']));              
              });

              info.addAll(subItems);

              return new Column(
                children: info,
              );
            },
          ),
        );
        break;
      case "classes":
        return new Master(
          title: name,
          child: new FirebaseAnimatedList(
            query: reference.orderByChild('name').equalTo(this.name),
            reverse: false,
            itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {

              var features = snapshot.value['features'];

              var subItems = new List<Widget>.generate(features.length, (int index) {
                return new EntryItem(new Entry(features[index]['name'], features[index]['description']));              
              });

              info.addAll(subItems);

              return new Column(
                children: info,
                mainAxisAlignment: MainAxisAlignment.start,
              );
            },
          ),
        );
        break;
        case "rules":
          return new Master(
            title: name,
            child: new FirebaseAnimatedList(
              query: reference.orderByChild('name').equalTo(this.name),
              reverse: false,
              itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {
                return new Column(
                  children: info,
                  mainAxisAlignment: MainAxisAlignment.start,
                );
              },
            ),
          );
        break;
        case "items":
          return new Master(
            title: name,
            child: new FirebaseAnimatedList(
              query: reference.orderByChild('name').equalTo(this.name),
              reverse: false,
              itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {
                return new Column(
                  children: info,
                  mainAxisAlignment: MainAxisAlignment.start,
                );
              },
            ),
          );
        break;
        case "spells":
          return new Master(
            title: name,
            child: new FirebaseAnimatedList(
              query: reference.orderByChild('name').equalTo(this.name),
              reverse: false,
              itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {
                return new Column(
                  children: info,
                  mainAxisAlignment: MainAxisAlignment.start,
                );
              },
            ),
          );
        break;
        case "monsters":
          return new Master(
            title: name,
            child: new FirebaseAnimatedList(
              query: reference.orderByChild('name').equalTo(this.name),
              reverse: false,
              itemBuilder: (_, DataSnapshot snapshot, Animation<double> animation) {
                return new Column(
                  children: info,
                  mainAxisAlignment: MainAxisAlignment.start,
                );
              },
            ),
          );
        break;
    }

    return new Master(
      title: name,
      child: new Container(
        padding: new EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 20.0),
        child: new Text("error - " + name + " - " + type),
      ),
    );
  }
}

class Entry {
  Entry(this.title, this.description);

  final String title;
  final String description;
}

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    return new ExpansionTile(
      key: new PageStorageKey<Entry>(root),
      initiallyExpanded: true,
      title: new Text(
          root.title,
          style: new TextStyle(
            color: redTheme,
            fontWeight: FontWeight.w600,
          ),
        ),
      children: [
        new  Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,     
          children: [
            new Padding(
              padding: new EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 20.0),
              child: new Text(
                root.description,
                textAlign: TextAlign.left,
                style: new TextStyle(
                
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}